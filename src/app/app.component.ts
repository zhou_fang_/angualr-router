import { Component } from '@angular/core';
import { slideInAnimation } from './animations';
import { RouterOutlet } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  animations: [slideInAnimation]
})
export class AppComponent {
  title = 'angular-router-sample';
  getAnimationData(outlet: RouterOutlet) {
    console.log(outlet);
    if (outlet && outlet.activatedRouteData) {
      console.log(outlet.activatedRouteData['animation']);
      return outlet.activatedRouteData['animation'];
    }
  }
}
