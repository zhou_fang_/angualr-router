/*
 * @Author: Zhou Fang
 * @Date: 2016-11-30 23:35:49
 * @Last Modified by:   Zhou Fang
 * @Last Modified time: 2018-11-30 23:35:49
 */


import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crisis-center',
  templateUrl: './crisis-center.component.html',
  styleUrls: ['./crisis-center.component.less']
})
export class CrisisCenterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
