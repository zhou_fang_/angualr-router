import { Component, OnInit } from '@angular/core';

import { CRISIS } from '../CRISIS';
import { CrisisService } from '../crisis.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-crisis-list',
  templateUrl: './crisis-list.component.html',
  styleUrls: ['./crisis-list.component.less']
})
export class CrisisListComponent implements OnInit {
  selectedId: number;
  crises: CRISIS[];
  crises$: Observable<CRISIS[]>;
  constructor(
    private crisisService: CrisisService,
    private route: ActivatedRoute
  ) {}
  getcrises(): void {
    this.crisisService.getCrises().subscribe(crises => (this.crises = crises));
  }
  ngOnInit() {
    // this.getcrises();
    this.crises$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.selectedId = +params.get('id');
        return this.crisisService.getCrises();
      })
    );
  }
}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
